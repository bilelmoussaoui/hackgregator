Code can be found in Gitlab:

    https://gitlab.com/gunibert/hackernews
    
Issues should be filed in Gitlab issue tracker

    https://gitlab.com/gunibert/hackernews/issues
    
Contributed code should stick to code style in the repository (its the same
code style used in most GNOME repositories)