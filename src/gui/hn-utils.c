/* hn-utils.c
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "hn-utils.h"

gchar *
html_2_pango (gchar *str)
{
  g_autofree gchar *tmp = str_replace (str, "<p>", "\n");
  tmp = str_replace (tmp, "</p>", "");
  tmp = str_replace (tmp, "rel=\"nofollow\"", "");
  tmp = str_replace (tmp, "<pre>", "");
  tmp = str_replace (tmp, "</pre>", "");
  tmp = str_replace (tmp, "<code>", "<tt>");
  tmp = str_replace (tmp, "</code>", "</tt>");

  return g_steal_pointer (&tmp);
}

gchar *
str_replace (gchar *str,
             gchar *toreplace,
             gchar *replace)
{
  g_autoptr(GRegex) regex = NULL;
  g_autofree gchar *escaped = NULL;

  g_return_val_if_fail (str != NULL, NULL);
  g_return_val_if_fail (toreplace != NULL, NULL);
  g_return_val_if_fail (replace != NULL, NULL);

  escaped = g_regex_escape_string (toreplace, -1);
  regex = g_regex_new (escaped, 0, 0, NULL);

  return g_regex_replace_literal (regex, str, -1, 0, replace, 0, NULL);
}
