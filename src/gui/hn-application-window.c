/* hn-application-window.c
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "hn-application-window.h"
#include "hn-article-page.h"
#include "hn-comment-row.h"
#include "hackernews-glib.h"
#include "hn-utils.h"
#define HANDY_USE_UNSTABLE_API
#include <handy.h>

struct _HnApplicationWindow
{
  GtkApplicationWindow parent_instance;

  /* ui */
  GtkWidget *headerbar;
  GtkWidget *leaflet;
  GtkWidget *back_btn;
  GtkWidget *refresh_btn;
  GtkWidget *comment_btn;
  GtkWidget *direct_link_btn;
  GtkWidget *stack;
  GtkWidget *item_list_box;
  GtkWidget *comment_list_box;
  GtkWidget *loading_spinner;
  GtkWidget *article_page;
  GtkWidget *itemtext;

  /* business */
  HnClient *client;
  GListStore *item_list;
  GListStore *comment_list;
};

G_DEFINE_TYPE (HnApplicationWindow, hn_application_window, GTK_TYPE_APPLICATION_WINDOW)

HnApplicationWindow *
hn_application_window_new (HnApplication *application)
{
  return g_object_new (HN_TYPE_APPLICATION_WINDOW,
                       "application", application,
                       NULL);
}

static void
hn_application_window_finalize (GObject *object)
{
  G_OBJECT_CLASS (hn_application_window_parent_class)->finalize (object);
}

static GtkWidget *
_create_comment_widget_cb (gpointer item,
                           gpointer user_data)
{
  GtkWidget *comment_row = hn_comment_row_new (item);

  return comment_row;
}

static gint
_stories_sort_func (gconstpointer a,
                    gconstpointer b)
{
  HnItem *item_a = *(HnItem **) a;
  HnItem *item_b = *(HnItem **) b;

  return hn_item_get_pos (item_a) - hn_item_get_pos (item_b);
}


static void
hn_application_window_fetched_comments (GObject      *source_object,
                                        GAsyncResult *res,
                                        gpointer      user_data)
{
  HnClient *client = HN_CLIENT (source_object);
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (user_data);

  //TODO: error handling
  GPtrArray *comments = hn_client_get_comments_finish (client, res, NULL);

  g_ptr_array_sort (comments, _stories_sort_func);
  for (guint i = 0; i < comments->len; i++) {
    HnItem *item = g_ptr_array_index (comments, i);
    if (hn_item_get_deleted (item)) continue;

    g_list_store_append (self->comment_list, g_steal_pointer (&item));
  }

  gtk_widget_hide (self->loading_spinner);
}

static void
hn_application_window_fetched_items (GObject      *source_object,
                                     GAsyncResult *res,
                                     gpointer      user_data)
{
  GError *error = NULL;
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (user_data);
  HnClient *client = HN_CLIENT (source_object);

  GPtrArray *topstories = hn_client_get_stories_finish (client, res, &error);
  if (error != NULL) {
    // handle error
  }

  g_ptr_array_sort (topstories, _stories_sort_func);

  gtk_widget_hide (self->loading_spinner);

  for (guint i = 0; i < topstories->len; i++) {
    HnItem *item = g_ptr_array_index (topstories, i);

    g_list_store_append (self->item_list, g_steal_pointer (&item));
  }
}
static void
on_comment_list_box_row_activated (GtkListBox    *listbox,
                                   GtkListBoxRow *row,
                                   gpointer       user_data)
{
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (user_data);
  // get comment row
  GtkWidget *comment_row = gtk_bin_get_child (GTK_BIN (row));

  hn_comment_row_reveal (HN_COMMENT_ROW (comment_row), self->client);
}

static void
_load_item_url (HnApplicationWindow *self,
                HnItem              *item)
{
  gtk_stack_set_visible_child_name (GTK_STACK (self->stack), "webview_page");
  hdy_leaflet_set_visible_child_name (HDY_LEAFLET (self->leaflet), "details_page");

  hn_article_page_set_active_item (HN_ARTICLE_PAGE (self->article_page), item);

  if (hdy_leaflet_get_fold (HDY_LEAFLET (self->leaflet)) == HDY_FOLD_FOLDED) {
    gtk_widget_show (self->back_btn);
    gtk_widget_hide (self->refresh_btn);
  }
  gtk_widget_show (self->comment_btn);
  gtk_widget_show (self->direct_link_btn);
}

static void
_load_item_text (HnApplicationWindow *self,
                 HnItem              *item)
{
  gtk_widget_show (self->itemtext);
  if (hn_item_get_text (item) == NULL)
    gtk_label_set_text (GTK_LABEL (self->itemtext), hn_item_get_title (item));
  else
    gtk_label_set_markup (GTK_LABEL (self->itemtext), html_2_pango (hn_item_get_text (item)));

  hn_client_get_comments_async (self->client, item, NULL, hn_application_window_fetched_comments, self);

  gtk_stack_set_visible_child_name (GTK_STACK (self->stack), "comment_page");
  hdy_leaflet_set_visible_child_name (HDY_LEAFLET (self->leaflet), "details_page");

  if (hdy_leaflet_get_fold (HDY_LEAFLET (self->leaflet)) == HDY_FOLD_FOLDED) {
    gtk_widget_show (self->back_btn);
    gtk_widget_hide (self->refresh_btn);
  }
  gtk_widget_show (self->loading_spinner);
}

static void
on_item_list_box_row_activated (GtkListBox    *listbox,
                                GtkListBoxRow *row,
                                gpointer       user_data)
{
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (user_data);
  gint index_ = gtk_list_box_row_get_index (row);
  HnItem *item = g_list_model_get_item (G_LIST_MODEL (self->item_list), index_);

  if (hn_item_get_url (item) == NULL)
    {
      g_list_store_remove_all (self->comment_list);
      _load_item_text (self, item);
    }
  else
    {
      gtk_widget_hide (self->itemtext);
      _load_item_url (self, item);
    }

}

static void
_load_comments_page (GtkButton *btn,
                     gpointer   user_data)
{
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (user_data);
  HnItem *item = NULL;
  g_list_store_remove_all (self->comment_list);

  if (g_strcmp0 (gtk_stack_get_visible_child_name (GTK_STACK (self->stack)), "webview_page") == 0) {
    item = hn_article_page_get_active_item (HN_ARTICLE_PAGE (self->article_page));
  } else {
    // find the ListBoxRow
    GtkWidget *parent = GTK_WIDGET (btn);

    while (!GTK_IS_LIST_BOX_ROW (parent))
      {
        parent = gtk_widget_get_parent (parent);

        if (parent == NULL)
          {
            g_error ("This should never be happen");
          }
      }

    gint index_ = gtk_list_box_row_get_index (GTK_LIST_BOX_ROW (parent));
    item = g_list_model_get_item (G_LIST_MODEL (self->item_list), index_);
  }

  if (hn_item_get_url (item) == NULL)
    {
      gtk_widget_show (self->itemtext);
      if (hn_item_get_text (item) == NULL)
        gtk_label_set_text (GTK_LABEL (self->itemtext), hn_item_get_title (item));
      else
        gtk_label_set_markup (GTK_LABEL (self->itemtext), html_2_pango (hn_item_get_text (item)));
    }
  else
    {
      gtk_widget_hide (self->itemtext);
    }

  hn_client_get_comments_async (self->client, item, NULL, hn_application_window_fetched_comments, self);

  hdy_leaflet_set_visible_child_name (HDY_LEAFLET (self->leaflet), "details_page");
  gtk_stack_set_visible_child_name (GTK_STACK (self->stack), "comment_page");
  if (hdy_leaflet_get_fold (HDY_LEAFLET (self->leaflet)) == HDY_FOLD_FOLDED)
    {
      gtk_widget_show (self->back_btn);
      gtk_widget_hide (self->refresh_btn);
    }
  gtk_widget_hide (self->comment_btn);
  gtk_widget_show (self->loading_spinner);

  hn_article_page_set_active_item (HN_ARTICLE_PAGE (self->article_page), item);
}

static void
on_refresh_clicked_cb (GtkButton *btn,
                       gpointer   user_data)
{
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (user_data);

  g_list_store_remove_all (self->item_list);
  gtk_widget_show (self->loading_spinner);
  hn_client_get_stories_async (self->client, -1, HN_TOP_STORIES, NULL, hn_application_window_fetched_items, self);
}

static void
back_btn_clicked_cb (GtkButton *btn,
                     gpointer   user_data)
{
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (user_data);

  hn_article_page_load_uri (HN_ARTICLE_PAGE (self->article_page), "about:blank");
  g_list_store_remove_all (self->comment_list);

  hdy_leaflet_set_visible_child_name (HDY_LEAFLET (self->leaflet), "item_page");

  gtk_widget_hide (self->back_btn);
  gtk_widget_hide (self->comment_btn);
  gtk_widget_hide (self->direct_link_btn);
  gtk_widget_show (self->refresh_btn);
}

/* Sections */
static void
hn_application_window_show_top_stories_cb (GtkButton *btn,
                                           gpointer   user_data)
{
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (user_data);

  g_list_store_remove_all (self->item_list);
  gtk_widget_show (self->loading_spinner);

  hdy_header_bar_set_title (HDY_HEADER_BAR (self->headerbar), "Top Stories");

  hn_client_get_stories_async (self->client, -1, HN_TOP_STORIES, NULL, hn_application_window_fetched_items, self);
}

static void
hn_application_window_show_new_stories_cb (GtkButton *btn,
                                           gpointer   user_data)
{
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (user_data);

  g_list_store_remove_all (self->item_list);
  gtk_widget_show (self->loading_spinner);

  hdy_header_bar_set_title (HDY_HEADER_BAR (self->headerbar), "New Stories");

  hn_client_get_stories_async (self->client, -1, HN_NEW_STORIES, NULL, hn_application_window_fetched_items, self);
}

static void
hn_application_window_show_ask_cb (GtkButton *btn,
                                   gpointer   user_data)
{
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (user_data);

  g_list_store_remove_all (self->item_list);
  gtk_widget_show (self->loading_spinner);
  hdy_header_bar_set_title (HDY_HEADER_BAR (self->headerbar), "Ask HN");

  hn_client_get_stories_async (self->client, -1, HN_ASK, NULL, hn_application_window_fetched_items, self);
}

static void
hn_application_window_show_show_cb (GtkButton *btn,
                                    gpointer   user_data)
{
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (user_data);

  g_list_store_remove_all (self->item_list);
  gtk_widget_show (self->loading_spinner);
  hdy_header_bar_set_title (HDY_HEADER_BAR (self->headerbar), "Show HN");

  hn_client_get_stories_async (self->client, -1, HN_SHOW, NULL, hn_application_window_fetched_items, self);
}

static void
hn_application_window_webview_load_changed (WebKitWebView   *web_view,
                                            WebKitLoadEvent  load_event,
                                            gpointer         user_data)
{
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (user_data);
  switch (load_event)
    {
    case WEBKIT_LOAD_STARTED:
      gtk_widget_show (self->loading_spinner);
      break;
    case WEBKIT_LOAD_FINISHED:
      gtk_widget_hide (self->loading_spinner);
      break;
    default:
      /* do nothing */
      break;
    }
}

static void
_direct_link_clicked_cb (GtkButton           *btn,
                         HnApplicationWindow *self)
{
  g_return_if_fail (HN_IS_APPLICATION_WINDOW (self));

  // get item
  HnItem *active_item = hn_article_page_get_active_item (self->article_page);
  gchar *url = hn_item_get_url (active_item);

  g_app_info_launch_default_for_uri (url, NULL, NULL);
}

static void
hn_application_window_class_init (HnApplicationWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = hn_application_window_finalize;

  gtk_widget_class_set_template_from_resource (widget_class, "/de/gunibert/Hackgregator/hn-application-window.ui");
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, back_btn);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, leaflet);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, refresh_btn);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, comment_btn);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, direct_link_btn);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, stack);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, item_list_box);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, comment_list_box);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, loading_spinner);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, article_page);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, headerbar);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, itemtext);
  gtk_widget_class_bind_template_callback (widget_class, on_item_list_box_row_activated);
  gtk_widget_class_bind_template_callback (widget_class, on_comment_list_box_row_activated);
  gtk_widget_class_bind_template_callback (widget_class, back_btn_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_refresh_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, _load_comments_page);
  gtk_widget_class_bind_template_callback (widget_class, _direct_link_clicked_cb);

  /* Sections */
  gtk_widget_class_bind_template_callback (widget_class, hn_application_window_show_top_stories_cb);
  gtk_widget_class_bind_template_callback (widget_class, hn_application_window_show_new_stories_cb);
  gtk_widget_class_bind_template_callback (widget_class, hn_application_window_show_ask_cb);
  gtk_widget_class_bind_template_callback (widget_class, hn_application_window_show_show_cb);
}

static GtkWidget *
_create_item_widget_cb (gpointer item,
                        gpointer user_data)
{
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (user_data);

  GtkWidget *box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 5);
  gtk_widget_set_margin_top (box, 5);
  gtk_widget_set_margin_bottom (box, 5);
  gtk_widget_set_margin_start (box, 5);
  gtk_widget_set_margin_end (box, 5);

  g_autofree gchar *pos_str = g_strdup_printf ("%d.", hn_item_get_pos (item) + 1);
  GtkWidget *position_lbl = gtk_label_new (pos_str);
  gtk_label_set_width_chars (GTK_LABEL (position_lbl), 3);
  gtk_label_set_xalign (GTK_LABEL (position_lbl), 0.0f);
  gtk_box_pack_start (GTK_BOX (box), position_lbl, FALSE, FALSE, 5);

  GtkWidget *lbl = gtk_label_new (hn_item_get_title (item));
  gtk_label_set_line_wrap (GTK_LABEL (lbl), TRUE);
  gtk_label_set_xalign (GTK_LABEL (lbl), 0.0f);

  gtk_box_pack_start (GTK_BOX (box), lbl, TRUE, TRUE, 5);

  GtkWidget *counter_btn = gtk_button_new ();
  gtk_button_set_relief (GTK_BUTTON (counter_btn), GTK_RELIEF_NONE);
  g_signal_connect (counter_btn, "clicked", G_CALLBACK (_load_comments_page), self);
  gtk_widget_set_valign (counter_btn, GTK_ALIGN_CENTER);

  g_autofree gchar *c = g_strdup_printf ("%d", hn_item_get_descendants (item));
  GtkWidget *counter = gtk_label_new (c);
  gtk_label_set_width_chars (GTK_LABEL (counter), 4);
  GtkStyleContext *context = gtk_widget_get_style_context (counter);
  gtk_style_context_add_class (context, "counter-label");
  gtk_widget_set_valign (counter, GTK_ALIGN_CENTER);

  gtk_container_add (GTK_CONTAINER (counter_btn), counter);
  gtk_box_pack_start (GTK_BOX (box), counter_btn, FALSE, FALSE, 5);

  gtk_widget_show_all (box);

  return box;
}

static void
hn_application_change_mode (GObject    *object,
                            GParamSpec *pspec,
                            gpointer    user_data)
{
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (user_data);
  HdyLeaflet *leaflet = HDY_LEAFLET (object);

  switch (hdy_leaflet_get_fold (leaflet))
    {
    case HDY_FOLD_FOLDED:
      if (g_strcmp0 (hdy_leaflet_get_visible_child_name (leaflet), "item_page") == 0)
        {
          gtk_widget_show (self->refresh_btn);
        }
      else
        {
          gtk_widget_hide (self->refresh_btn);
          gtk_widget_show (self->back_btn);
          if (g_strcmp0 (hdy_leaflet_get_visible_child_name (leaflet), "webview_page") == 0)
            {
              gtk_widget_show (self->comment_btn);
            }
          else
            {
              gtk_widget_hide (self->comment_btn);
            }
        }
      break;
    case HDY_FOLD_UNFOLDED:
      gtk_widget_hide (self->back_btn);
      gtk_widget_show (self->refresh_btn);
      gtk_widget_show (self->direct_link_btn);
      break;
    }
}

static void
hn_application_window_init (HnApplicationWindow *self)
{
  g_type_ensure (HDY_TYPE_COLUMN);
  g_type_ensure (HN_TYPE_ARTICLE_PAGE);
  gtk_widget_init_template (GTK_WIDGET (self));

  // setup leaflet
  hdy_leaflet_set_mode_transition_type (HDY_LEAFLET (self->leaflet), HDY_LEAFLET_MODE_TRANSITION_TYPE_SLIDE);
  hdy_leaflet_set_child_transition_type (HDY_LEAFLET (self->leaflet), HDY_LEAFLET_CHILD_TRANSITION_TYPE_SLIDE);

  self->client = hn_client_new ("https://hacker-news.firebaseio.com/v0");
  self->item_list = g_list_store_new (HN_TYPE_ITEM);
  self->comment_list = g_list_store_new (HN_TYPE_ITEM);

  GtkWidget *lbl = gtk_label_new ("Nothing to see here");
  gtk_list_box_set_placeholder (GTK_LIST_BOX (self->comment_list_box), lbl);
  gtk_widget_show (lbl);

  WebKitWebView *webview = hn_article_page_get_webview (HN_ARTICLE_PAGE (self->article_page));
  WebKitSettings *webview_settings = webkit_web_view_get_settings (webview);
  webkit_settings_set_user_agent (webview_settings, "Mozilla/5.0 (Linux; U; Android 4.4.2; en-us; SCH-I535 Build/KOT49H) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
  g_signal_connect (webview,
                    "load-changed",
                    G_CALLBACK (hn_application_window_webview_load_changed),
                    self);

  g_signal_connect (self->leaflet, "notify::fold", G_CALLBACK (hn_application_change_mode), self);

  gtk_list_box_bind_model (GTK_LIST_BOX (self->item_list_box), G_LIST_MODEL (self->item_list),
                           _create_item_widget_cb, self, NULL);
  gtk_list_box_bind_model (GTK_LIST_BOX (self->comment_list_box), G_LIST_MODEL (self->comment_list),
                           _create_comment_widget_cb, self, NULL);

  hn_client_get_stories_async (self->client, -1, HN_TOP_STORIES, NULL, hn_application_window_fetched_items, self);
}
