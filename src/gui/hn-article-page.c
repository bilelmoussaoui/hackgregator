/* hn-article-page.c
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "hn-article-page.h"

struct _HnArticlePage
{
  GtkScrolledWindow parent_instance;

  GtkWidget *webview;
  HnItem *active_item;
};

G_DEFINE_TYPE (HnArticlePage, hn_article_page, GTK_TYPE_SCROLLED_WINDOW)

enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

HnArticlePage *
hn_article_page_new (void)
{
  return g_object_new (HN_TYPE_ARTICLE_PAGE, NULL);
}

static void
hn_article_page_finalize (GObject *object)
{
  HnArticlePage *self = (HnArticlePage *)object;

  G_OBJECT_CLASS (hn_article_page_parent_class)->finalize (object);
}

static void
hn_article_page_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  HnArticlePage *self = HN_ARTICLE_PAGE (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
hn_article_page_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  HnArticlePage *self = HN_ARTICLE_PAGE (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
hn_article_page_class_init (HnArticlePageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = hn_article_page_finalize;
  object_class->get_property = hn_article_page_get_property;
  object_class->set_property = hn_article_page_set_property;

  gtk_widget_class_set_template_from_resource (widget_class, "/de/gunibert/Hackgregator/hn-article-page.ui");
  gtk_widget_class_bind_template_child (widget_class, HnArticlePage, webview);
}

static void
hn_article_page_init (HnArticlePage *self)
{
  g_type_ensure (WEBKIT_TYPE_WEB_VIEW);
  gtk_widget_init_template (GTK_WIDGET (self));
}

void
hn_article_page_set_active_item (HnArticlePage *self,
                                 HnItem        *item)
{
  g_return_if_fail (HN_IS_ARTICLE_PAGE (self));
  g_return_if_fail (HN_IS_ITEM (item));

  self->active_item = item;
  hn_article_page_load_uri (self, hn_item_get_url (self->active_item));
}

HnItem *
hn_article_page_get_active_item (HnArticlePage *self)
{
  g_return_val_if_fail (HN_IS_ARTICLE_PAGE (self), NULL);

  return self->active_item;
}

void
hn_article_page_load_uri (HnArticlePage *self,
                          gchar         *uri)
{
  g_return_if_fail (HN_IS_ARTICLE_PAGE (self));

  webkit_web_view_load_uri (WEBKIT_WEB_VIEW (self->webview), uri);
}

WebKitWebView *
hn_article_page_get_webview (HnArticlePage *self)
{
  g_return_val_if_fail (HN_IS_ARTICLE_PAGE (self), NULL);

  return WEBKIT_WEB_VIEW (self->webview);
}
