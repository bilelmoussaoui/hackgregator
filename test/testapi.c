/* testapi.c
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <glib.h>
#include "hackernews.h"
#include "server/hn-mock-server.h"

void
test_api_create (void)
{
  HnClient *client = hn_client_new ("https://hacker-news.firebaseio.com/v0/");

  g_assert_nonnull (client);
}

void
test_get_item (void)
{
  hn_mock_server_start ();

  //HnClient *client = hn_client_new ("https://hacker-news.firebaseio.com/v0");
  HnClient *client = hn_client_new ("http://localhost:10000");

  HnItem *item = hn_client_get_item (client, 8863);
  g_assert_nonnull (item);

  gchar *title = hn_item_get_title (item);
  g_assert_cmpstr (title , ==, "My YC app: Dropbox - Throw away your USB drive");

  gchar *url = hn_item_get_url (item);
  g_assert_cmpstr (url, ==, "http://www.getdropbox.com/u/2/screencast.html");

  gchar *author = hn_item_get_author (item);
  g_assert_cmpstr (author, ==, "dhouston");

  GDateTime *creation_time = hn_item_get_creation_time (item);
  g_assert_cmpint (g_date_time_to_unix (creation_time), ==, 1175714200);

  hn_mock_server_stop ();
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/api/create", test_api_create);
  g_test_add_func ("/api/get_item", test_get_item);

  return g_test_run ();
}
