/* benchmark.c
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <glib.h>
#include "hackernews.h"


void
benchmark_client_cb (GObject       *source_object,
                     GAsyncResult *res,
                     gpointer      user_data)
{
  GMainLoop *loop = user_data;
  GPtrArray *items = hn_client_get_top_stories_finish (HN_CLIENT (source_object), res, NULL);

  g_main_loop_quit (loop);
}

gboolean
benchmark_client (gpointer user_data)
{
  GMainLoop *loop = user_data;
  HnClient *client = hn_client_new ("https://hacker-news.firebaseio.com/v0");

  hn_client_get_top_stories_async (client, -1, NULL, benchmark_client_cb, loop);

  return G_SOURCE_REMOVE;
}

gint
main (gint   argc,
      gchar *argv[])
{
  GMainLoop *loop = g_main_loop_new (NULL, FALSE);

  g_idle_add (benchmark_client, loop);

  g_main_loop_run (loop);
}
